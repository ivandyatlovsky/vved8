﻿#include <iostream>
#include "constants.h"
#include "precipitation.h"
#include "file_reader.h"
using namespace std;
int main() {
	setlocale(LC_ALL, "ru");
	cout << "Lab rab 8. GIT\n";
	cout << "Var 3. Osadki\n";
	cout << "Avtor: Ivan Dytlovski\n\n";
	precipitation* pre[MAX_STRING_SIZE];
	int size;
	try
	{
		read("Vanya.txt", pre, size);
		for (int i = 0; i < size; i++)
		{
			cout << "*****Prognoz*****" << endl;
			cout<<"Data: " << pre[i]->day << " : ";
			cout << pre[i]->month << '\n';
			cout<<"Kolichestvo: " << pre[i]->number << '\n';
			cout << pre[i]->type << '\n';
			cout << endl;
		}
		for (int i = 0; i < size; i++)
		{
			delete pre[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}